package jp.alhinc.sato_ayumi.calculate_sales;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class CalculateSales {

	public static void main(String[] args) {
		Map<String, String> branchName = new HashMap<>();  //HashMapの宣言 branchName
		Map<String, Long> branchSales = new HashMap<>();   //HashMapの宣言 branchSales
		
		
		BufferedReader br = null;
		try {
			File file = new File(args[0],"branch.lst");   //args[0]内のbranch.lstの情報を読み込み
			FileReader fr = new FileReader(file);   
			br = new BufferedReader(fr);
			
				String line;
				while((line = br.readLine()) != null) {    //nullになるまで繰り返す
				String[] items = line.split(",");   //spritで区切る
					branchName.put(items[0],items[1]);   //Map branchNameにkey支店コード、Value店舗名を指定し、値を保持する
					branchSales.put(items[0],0L);       //Map branchNameにkey支店コード、Value0(long型)を指定し、値を保持する
				}
				
			
				File[] files = new File(args[0]).listFiles();  //listFileメソッドを使用しargs[0]内のファイル情報を取得
				for(int i = 0; i < files.length ; i++) {    //filesの数だけ繰り返す
					String fileName = files[i].getName();     //.getName();でファイル名取得
					
					if(fileName.matches("\\d{8}.*rcd")) 
						System.out.println(fileName);
				
					
					
				}
				
		}catch(IOException e) {                  //IOExceptionを取得したら
			System.out.println("File not found");    
			
		}finally {
			if(br != null) {         //brにBufferedReaderの読み込みが完了し、nullが返されたら
				try {
					br.close();       //BufferedReaderで読み込んだfr、branch.lstを閉じる
				}catch(IOException e) {         //IOExceptionを取得したら
					System.out.println("Can't close file");
				}	
			}
		}
	}
}