package jp.alhinc.sato_ayumi.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	public static void main(String[] args) {


		//エラー処理 コマンドライン引数が指定されているか
		if(args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
			return;

		}

		//HashMapの宣言 branchName
		Map<String, String> branchName = new HashMap<>();

		//HashMapの宣言 branchSales
		Map<String, Long> branchSales = new HashMap<>();

		//HashMapの宣言 commodityName
		Map<String, String> commodityName = new HashMap<>();

		//HashMapの宣言 commoditySales
		Map<String, Long> commoditySales = new HashMap<>();


		if(!inputFile(args[0] , "branch.lst" , branchName , branchSales ,
				"支店定義","\\d{3}")){
			System.out.println("予期せぬエラーが発生しました");
			return;
		}

		if(!inputFile(args[0] , "commodity.lst" , commodityName , commoditySales ,
				"商品定義","[0-9A-Z]{8}")){
			System.out.println("予期せぬエラーが発生しました");
			return;
		}

		//売上ファイル読み込み

		//listFileメソッドを使用しargs[0]内のファイル情報を取得
		File[] files = new File(args[0]).listFiles();

		//ArrayList rcdFilesを宣言
		List<File> rcdFiles = new ArrayList<File>();

		//filesの数だけ繰り返す
		for(int i = 0; i < files.length ; i++) {

			//.getName();でファイル名取得
			String fileNames = files[i].getName();

			//数字8桁、末尾rcdのファイルを検索
			if(files[i].isFile() && fileNames.matches("\\d{8}.*rcd")) {

				//ArrayList rcdFilesへ、一致したファイルのパスを格納
				rcdFiles.add(files[i]);
			}
		}

		//Listのソート MacOS対策
		Collections.sort(rcdFiles);

		//エラー処理 ファイル名連番確認
		for(int i = 0; i < rcdFiles.size() - 1; i++) {

			//rcdがついたファイルのパスから名前を抜き取り、前8桁を取り出す
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0,8));

			//rcdがついた次のファイルのパスから名前を抜き取り、前8桁を取り出す
			int latter = Integer.parseInt((rcdFiles.get(i+1)).getName().substring(0,8));

			//差分が1にならなければ連番になっていない
			if((latter - former) != 1) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}

		BufferedReader br = null;

		//rcdFilesの数だけ繰り返す
		for(int i = 0; i < rcdFiles.size() ; i++) {

			try{
				//rcdFiles(売上ファイルの中身)を読み込み。
				//rcdFilesはFileの要素を持っているのでインスタンスは必要ない
				File file = rcdFiles.get(i);

				FileReader fr = new FileReader(file);
				br = new BufferedReader(fr);
				String line;

				//ArrayList(String型）rcdLineを生成
				List<String> rcdLine = new ArrayList<String>();

				//nullになるまで繰り返す 1行ずつ追加していく
				while((line = br.readLine()) != null) {
					rcdLine.add(line);
				}

				//エラー処理 3行以上になっていないか確認
				if(rcdLine.size() != 3) {
					System.out.println(rcdFiles.get(i).getName() + "のフォーマットが不正です");
					return;
				}

				//エラー処理 記載された支店コードが支店定義ファイルの中に存在するか確認
				if(!branchName.containsKey(rcdLine.get(0))) {
					System.out.println(rcdFiles.get(i).getName() + "の支店コードが不正です");
					return;
				}

				//エラー処理 記載された商品コードが商品定義ファイルの中に存在するか確認
				if(!commodityName.containsKey(rcdLine.get(1))) {
					System.out.println(rcdFiles.get(i).getName() + "の商品コードが不正です");
					return;
				}

				//エラー処理 3行目の売上金額が数字であるか確認
				if(!rcdLine.get(2).matches("^[0-9]+$")) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}

				//キャスト 売上ファイル3行目(rcdLine.get(2)) StringなのでLong型へ
				long fileSale = Long.parseLong(rcdLine.get(2));

				//Long型のbranchSalesAmountを作成、branchSalesから(支店番号)取得し、売り上げ金額を足す
				Long branchSalesAmount = branchSales.get(rcdLine.get(0)) + fileSale;

				//Long型のcommoditySalesAmountを作成、commoditySalesから(商品コード)取得し、売り上げ金額を足す
				Long commoditySalesAmount = commoditySales.get(rcdLine.get(1)) + fileSale;

				//エラー処理 10桁以上になった場合表示
				if(branchSalesAmount >= 10000000000L){
					System.out.println("支店別売上の合計金額が10桁を超えました");
					return;
				}

				//エラー処理 10桁以上になった場合表示
				if(commoditySalesAmount >= 10000000000L){
					System.out.println("商品別売上の合計金額が10桁を超えました");
					return;
				}

				//branchSalesのMapに値を戻す。
				//繰り返しているので、同じ支店番号の値は加算され上書きされている。
				branchSales.put(rcdLine.get(0),branchSalesAmount);

				//commoditySalesのMapに値を戻す。
				commoditySales.put(rcdLine.get(1),commoditySalesAmount);
			}

			catch(IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			}

			finally {
				//brにBufferedReaderの読み込みが完了し、nullが返されたら
				if(br != null) {

					try {
						//BufferedReaderで読み込んだfr、branch.lstを閉じる
						br.close();
					}

					catch(IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
				}
			}
		}

		if(!outputFile(args[0] , "branch.out" , branchName , branchSales)){
			System.out.println("予期せぬエラーが発生しました");
			return;
		}

		if(!outputFile(args[0] , "commodity.out" , commodityName , commoditySales)){
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
	}


   //定義ファイル読み込み
	public static boolean inputFile( String filePath ,
			 String fileName , Map<String, String> nameMap , Map<String, Long> salesMap ,
			 String errorName , String digit) {

		BufferedReader br = null;

		//定義ファイル読み込み
		try {
			//mainメッソドから渡された引数の情報を読み込み
			File file = new File(filePath , fileName);

			//エラー処理 定義ファイルの存在確認
			if(!file.exists()) {
				System.out.println(errorName + "ファイルが存在しません");
				return false;
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			String line;

			//nullになるまで繰り返す
			while((line = br.readLine()) != null) {

				//spritで区切る
				String[] items = line.split(",");

				//エラー処理 定義ファイルのフォーマット
				if((items.length != 2) || (!items[0].matches(digit))) {
					System.out.println(errorName + "ファイルのフォーマットが不正です");
					return false;
				}

				//Map branchNameにkeyコード、Value名称を指定し、値を保持する
				nameMap.put(items[0],items[1]);

				//Map branchNameにkeyコード、Value0(long型)を指定し、値を保持する
				salesMap.put(items[0],0L);
			}
		}

		catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}

		finally {
			//brにBufferedReaderの読み込みが完了し、nullが返されたら
			if(br != null) {
				try {
					//BufferedReaderで読み込んだfr、branch.lstを閉じる
					br.close();
				}

				catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}

		return true;
	}


	//集計結果書き込み
	public static boolean outputFile(String filePath , String fileName ,
			Map<String, String> nameMap , Map<String, Long> salesMap ) {

		BufferedWriter bw = null;

		try {

			//mainメッソドから渡された引数の情報を読み込み
			File file = new File(filePath , fileName);

			FileWriter fw = new FileWriter(file);

			bw = new BufferedWriter(fw);

			for(String key : nameMap.keySet()) {

				bw.write(key+"," + nameMap.get(key)+"," + salesMap.get(key));

				//全て記入したら改行
				bw.newLine();
			}
		}

		catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}

		finally {
			//brにBufferedReaderの読み込みが完了し、nullが返されたら
			if(bw != null) {

				try {
					//BufferedReaderで読み込んだfr、branch.lstを閉じる
					bw.close();
				}

				catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}

		return true;
	}
}
